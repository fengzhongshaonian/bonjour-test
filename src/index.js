
const options = {
    multicast: true, // use udp multicasting
    interface: '192.168.1.104', // explicitly specify a network interface. defaults to all
    port: 5353, // set the udp port
    // ip: '224.0.0.251', // set the udp ip
    ttl: 255, // set the multicast ttl
    loopback: false, // receive your own packets
    reuseAddr: true // set the reuseAddr option when creating the socket (requires node >=0.11.13)
}


const bonjour = require('bonjour')(options);

// advertise an HTTP server on port 3000
bonjour.publish({ name: 'My Web Server1', type: 'http', port: 3000 })
bonjour.publish({ name: 'My Web Server2', type: 'http', port: 3001 })
bonjour.publish({ name: 'My Web Server3', type: 'http', port: 3002 })
bonjour.publish({ name: 'My Web Server4', type: 'http', port: 3003 })
bonjour.publish({ name: 'My Web Server5', type: 'http', port: 3004 })

// browse for all http services
bonjour.find({ type: 'http' }, function (service) {
    console.log('Found an HTTP server:', service)
})